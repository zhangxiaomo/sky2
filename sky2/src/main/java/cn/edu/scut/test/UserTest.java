package cn.edu.scut.test;

import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cn.edu.scut.dao.StudentDao;
import cn.edu.scut.dao.UserDao;
import cn.edu.scut.model.Student;
import cn.edu.scut.model.User;


@RunWith(SpringJUnit4ClassRunner.class)
public class UserTest {

	@Autowired
	private StudentDao studentDao;
	@Test
	public void testAdd() {
		Student student = new Student();
		student.setStudent_id("2");
		student.setPhone("18271182819");
		student.setPassword("1234");
		student.setNickname("小莫");
		student.setState("1");
		studentDao.add(student);
	}

	@Test
	public void testDelete() {
		fail("Not yet implemented");
	}

	@Test
	public void testUpdate() {
		fail("Not yet implemented");
	}

	@Test
	public void testGet() {
		Student student = studentDao.get("1");
		System.out.println(student);
	}

	@Test
	public void testQuery() {
		fail("Not yet implemented");
	}

	@Test
	public void testCount() {
		fail("Not yet implemented");
	}


}
