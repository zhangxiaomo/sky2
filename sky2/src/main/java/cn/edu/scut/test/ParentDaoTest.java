package cn.edu.scut.test;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cn.edu.scut.dao.ParentDao;
import cn.edu.scut.model.Parent;
@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration("classpath:applicationContext-dao.xml")
@Service
public class ParentDaoTest {

	@Autowired
	private ParentDao parentDao;
	@Test
	public void testGetParentById() {
		Parent parent = parentDao.getParentById(1l);
		System.out.println(parent);
	}

	@Test
	public void testInsertParent() {
		fail("Not yet implemented");
	}

	@Test
	public void testUpdateParent() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteParent() {
		fail("Not yet implemented");
	}

	@Test
	public void testQuery() {
		fail("Not yet implemented");
	}

}
