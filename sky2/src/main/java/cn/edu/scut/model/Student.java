package cn.edu.scut.model;

import java.util.Date;

public class Student{

	private String student_id;
	private String phone;
	private String password;
	private Date create_time;
	private String pic_path;
	private String nickname;
	private Date birthday;
	private String sex;
	private String school_id;
	private String token;
	private String state;
	private String verify_code;
	public String getStudent_id() {
		return student_id;
	}
	public void setStudent_id(String student_id) {
		this.student_id = student_id;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Date getCreate_time() {
		return create_time;
	}
	public void setCreate_time(Date create_time) {
		this.create_time = create_time;
	}
	public String getPic_path() {
		return pic_path;
	}
	public void setPic_path(String pic_path) {
		this.pic_path = pic_path;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getSchool_id() {
		return school_id;
	}
	public void setSchool_id(String school_id) {
		this.school_id = school_id;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getVerify_code() {
		return verify_code;
	}
	public void setVerify_code(String verify_code) {
		this.verify_code = verify_code;
	}
	@Override
	public String toString() {
		return "Student [student_id=" + student_id + ", phone=" + phone + ", password=" + password + ", create_time="
				+ create_time + ", pic_path=" + pic_path + ", nickname=" + nickname + ", birthday=" + birthday
				+ ", sex=" + sex + ", school_id=" + school_id + ", token=" + token + ", state=" + state
				+ ", verify_code=" + verify_code + "]";
	}
	
	
	
}
