package cn.edu.scut.model;

public class Parent {

	private Long parent_id;
	private Long phone;
	private Long token;
	private Long password;
	private Long name;
	private Long nickname;
	private Long pic_path;
	private Long birthday;
	private Long gender;
	private Long state;
	private Long create_time;
	public Long getParent_id() {
		return parent_id;
	}
	public void setParent_id(Long parent_id) {
		this.parent_id = parent_id;
	}
	public Long getPhone() {
		return phone;
	}
	public void setPhone(Long phone) {
		this.phone = phone;
	}
	public Long getToken() {
		return token;
	}
	public void setToken(Long token) {
		this.token = token;
	}
	public Long getPassword() {
		return password;
	}
	public void setPassword(Long password) {
		this.password = password;
	}
	public Long getName() {
		return name;
	}
	public void setName(Long name) {
		this.name = name;
	}
	public Long getNickname() {
		return nickname;
	}
	public void setNickname(Long nickname) {
		this.nickname = nickname;
	}
	public Long getPic_path() {
		return pic_path;
	}
	public void setPic_path(Long pic_path) {
		this.pic_path = pic_path;
	}
	public Long getBirthday() {
		return birthday;
	}
	public void setBirthday(Long birthday) {
		this.birthday = birthday;
	}
	public Long getGender() {
		return gender;
	}
	public void setGender(Long gender) {
		this.gender = gender;
	}
	public Long getState() {
		return state;
	}
	public void setState(Long state) {
		this.state = state;
	}
	public Long getCreate_time() {
		return create_time;
	}
	public void setCreate_time(Long create_time) {
		this.create_time = create_time;
	}
	@Override
	public String toString() {
		return "Parent [parent_id=" + parent_id + ", phone=" + phone + ", token=" + token + ", password=" + password
				+ ", name=" + name + ", nickname=" + nickname + ", pic_path=" + pic_path + ", birthday=" + birthday
				+ ", gender=" + gender + ", state=" + state + ", create_time=" + create_time + "]";
	}
	
}
