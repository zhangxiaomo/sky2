package cn.edu.scut.service;

import java.util.List;

import cn.edu.scut.model.User;

public interface IUserService {
	public void add(User user);
	public void delete(int id);
	public void update(User user);
	public User get(int id);
	public List<User> query();
	public int count();
}
