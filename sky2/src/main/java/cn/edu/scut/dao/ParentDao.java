package cn.edu.scut.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.edu.scut.model.Parent;

public interface ParentDao {

		public Parent getParentById(@Param("parentId")Long parentId);
		
		public void insertParent(@Param("parent") Parent parent);

		public void updateParent(@Param("parent")Parent parent);
		
		public void deleteParent(@Param("parent")Parent parent);
		public List<Parent> query();
	

}
