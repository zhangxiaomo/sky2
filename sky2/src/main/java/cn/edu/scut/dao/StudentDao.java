package cn.edu.scut.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import cn.edu.scut.model.Student;

@Repository
public interface StudentDao {

	public void add(Student student);
	public void delete(@Param("id") int id);
	public void update(Student student);
	public Student get(String string);
	public List<Student> query();
	public int count();
}
